import pyautogui
from threading import Thread
from pygame.locals import *
from gui.button import *
from game.game import *
from game.solution import Solution


class Main:

    APP_FONT = "Arial"

    def __init__(self):
        pygame.init()
        clock = pygame.time.Clock()

        # Entire screen
        self.screenWidth, self.screenHeight = pyautogui.size()
        self.screen = pygame.display.set_mode([self.screenWidth, self.screenHeight-70], RESIZABLE)
        # self.screen = pygame.display.set_mode([self.screenWidth/2, self.screenHeight-70], RESIZABLE)
        pygame.display.set_caption("Rush Hour - Killian's revisted version")

        self.buttonsDict = {}
        self.windowContent = pygame.Surface((self.screenWidth, self.screenHeight))
        self.windowContent.fill("#757171") # Grey

        # Board
        self.board = Board((800, 800), (50, 50), (6, 6))

        # Game intelligence
        self.game = Game(self.board)
        self.game.generateGame(Difficulty.EASY) # Add the vehicles

        # Game resolution
        self.solution = Solution(self.game, self.board)
        self.solution.createGraph()

        # Game options
        self.createGameOptionsPart()

        running = True
        while running:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False

                # Mouse events
                if event.type == MOUSEBUTTONDOWN:
                    if event.button == 1:  # 1 == left button
                        self.game.mouseClick()

                    [button.catchEvent(event) for button in self.buttonsDict.values()]

                # KeyBoard events
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        self.game.keyPressed(pygame.K_LEFT)
                    if event.key == pygame.K_RIGHT:
                        self.game.keyPressed(pygame.K_RIGHT)
                    if event.key == pygame.K_UP:
                        self.game.keyPressed(pygame.K_UP)
                    if event.key == pygame.K_DOWN:
                        self.game.keyPressed(pygame.K_DOWN)

            self.game.update()
            self.board.displayOn(self.windowContent)
            self.screen.blit(self.windowContent, (0, 0)) # Displays the content to the screen

            pygame.display.update()

            # Frames per second
            clock.tick(60)

    # def addTaskBar(self, height):
    #     pygame.draw.rect(self.screen, (0, 0, 0), pygame.Rect(0, 0, self.screenWidth, height))
    #
    #     # Window title
    #     title = pygame.font.SysFont(self.APP_FONT, 20).render("Rush hour version pygame", True, "White")
    #     self.screen.blit(title, (10, 0))
    #
    #     # Close button
    #     closeButton = MyButton("Fermer", (0, 0), 20, "", "White", self.close)
    #     closeButton.setPosX(self.screen.get_width() - closeButton.getSize()[0] * 1.2)  # To space the button from the right side of the window
    #     closeButton.displayOn(self.screen)
    #     return closeButton

    def createGameOptionsPart(self):
        title = pygame.font.SysFont(self.APP_FONT, 100).render("Rush hour", True, "White")
        self.windowContent.blit(title, (self.screenWidth/1.65, self.screenHeight/8))
        
        solutionMovesNumber = pygame.font.SysFont(self.APP_FONT, 20).render("Solution en " + str(len(self.solution.getVictoryMoves())) + " coups trouvée [XX:XX ms].", True, "White")
        self.windowContent.blit(solutionMovesNumber, (self.screenWidth/2, self.screenHeight/3))

        showWinningStepsButton = MyButton("Montrer la solution", (self.screenWidth/2, self.screenHeight/3), 20, "Black", "White", self.showSolution)
        showWinningStepsButton.setPosX(self.screenWidth - showWinningStepsButton.getSize()[0] * 1.2)
        showWinningStepsButton.displayOn(self.windowContent)
        self.buttonsDict["winningSteps"] = showWinningStepsButton

    @staticmethod
    def close():
        pygame.quit()
        exit()

    def showSolution(self):
        # TODO:
        #   Faire en sorte de pouvoir changer le texte d'un bouton. Voir pour le supprimer ou avoir 2 boutons caché/affichés avec une action chacun.
        #   Une méthode update ? -> https://stackoverflow.com/questions/46719847/updating-text-in-pygame
        # self.buttonsDict["winningSteps"].setText("Arrêter")
        # self.buttonsDict["winningSteps"].displayOn(self.windowContent)
        animationThread = Thread(target=self.solution.animateSolution)  # ...Instancie un thread et lui donne un ID unique
        animationThread.start()


if __name__ == '__main__':
    Main()

import pygame
from gui.board import Board


class Colors:
    """
    This class list all the selectable colors for a vehicle.
    """

    BLUE = "#4473c5"
    LIGHTBLUE = "#01b0f1"
    CYAN = "#0c9b9f"
    GREEN = "#395723"
    LIGHTGREEN = "#70ad46"
    BROWN = "#7e6000"
    ORANGE = "#ed7d31"
    PURPLE = "#7030a0"
    YELLOW = "#ffc000"
    RED = "Red"
    PINK = "#ea005f"
    GREY = "#a6a6a6"
    BLACK = "#0d0c0d"
    CREAM = "#dbccab"

    COLORS_LIST = [BLUE, LIGHTBLUE, CYAN, GREEN, LIGHTGREEN, BROWN, ORANGE, PURPLE, YELLOW, RED, PINK, GREY, BLACK, CREAM]

    def __init__(self):
        pass


class Orientations:
    """
    Corresponds to the head orientation of the vehicle.
    First the vehicle is drawn from left to right and then rotated to the desired orientation
    """

    TO_LEFT = 180
    TO_RIGHT = 0
    TO_BOTTOM = -90
    TO_TOP = 90

    ORIENTATIONS_LIST = [TO_LEFT, TO_RIGHT, TO_BOTTOM, TO_TOP]

    def __init__(self):
        pass


class Vehicle(pygame.sprite.Sprite):
    """
    This class represent a vehicle in the game.
    It need to be inherited to implement a specific appearance.
    """
    WIDTH_MARGIN = 24
    HEIGHT_MARGIN = 16

    def __init__(self, vehicleId: int, name: str, color: str, cellSize: int, board: Board, orientation: Orientations, cellPosition: tuple[int, int]):
        super().__init__()

        if color not in Colors.COLORS_LIST:
            raise Exception("The color : " + str(color) + " is not included in the COLORS list of the vehicle class")
        if orientation not in Orientations.ORIENTATIONS_LIST:
            raise Exception("The orientation provided doesn't correspond to one of the acceptable possibilities : see Vehicle class")

        self._id = vehicleId
        self._name = name
        self._color = color
        self._board = board
        self._cellSize = cellSize
        self._vehicleWidth, self._vehicleHeight = ((self._cellSize * self._board.getCellWidth()) - Vehicle.WIDTH_MARGIN), (self._board.getCellHeight() - Vehicle.HEIGHT_MARGIN) # Corresponds to the vehicle drawing size
        self._orientation = orientation
        self._cellPosition = cellPosition

        # Drawing
        self.image = None

    @staticmethod
    def getOrientationName(orientationValue: int) -> str:
        if orientationValue == Orientations.TO_LEFT:
            return "TO_LEFT"
        if orientationValue == Orientations.TO_RIGHT:
            return "TO_RIGHT"
        if orientationValue == Orientations.TO_BOTTOM:
            return "TO_BOTTOM"
        if orientationValue == Orientations.TO_TOP:
            return "TO_TOP"

    def getId(self):
        return self._id

    def getName(self):
        """
        Returns the name of the vehicle.
        """
        return self._name

    def getColor(self) -> str:
        """
        Returns the color of the vehicle.
        """
        return self._color

    def getCellSize(self) -> int:
        """
        Returns the number of board cells that this vehicle is taking.
        It represent the 'vehicle size'.
        """
        return self._cellSize

    def getOrientation(self) -> Orientations:
        """
        Returns the orientation of the vehicle.
        """
        return self._orientation

    def getPosition(self) -> tuple[int, int]:
        """
        Returns the coordinates of the vehicle in the board.
        """
        return self._cellPosition

    def isVertical(self) -> bool:
        """
        Returns true if the vehicle is placed vertically false otherwise.
        This means the vehicle is heading to_top or to_bottom.
        """
        return self._orientation == Orientations.TO_TOP or self._orientation == Orientations.TO_BOTTOM

    def isHorizontal(self) -> bool:
        """
        Return true if th vehicle is placed horizontally, false otherwise.
        This means the vehicle is heading to_left or to_right.
        """
        return not self.isVertical()

    def rotate(self, angle: Orientations = 0):
        """
        Rotates the vehicle at the ange specified in parameter.
        """
        # noinspection PyTypeChecker
        self.image = pygame.transform.rotate(self.image, angle)

    def setPosX(self, newPosX: int):
        if newPosX is None:
            return
        self.rect.x = newPosX
        self._cellPosition = (newPosX, self._cellPosition[1])

    def setPosY(self, newPosY: int):
        if newPosY is None:
            return
        self.rect.y = newPosY
        self._cellPosition = (self._cellPosition[0], newPosY)

    def setPos(self, newPosX: int, newPosY: int):
        self.setPosX(newPosX)
        self.setPosY(newPosY)

    def moveRight(self):
        """
        Moves the vehicule from one board cell to right.
        """
        self.setPosX(self.rect.x + self._board.getCellWidth())

    def moveLeft(self):
        """
        Moves the vehicule from one board cell to left.
        """
        self.setPosX(self.rect.x - self._board.getCellWidth())

    def moveUp(self):
        """
        Moves the vehicule from one board cell to up.
        """
        self.setPosY(self.rect.y - self._board.getCellHeight())

    def moveDown(self):
        """
        Moves the vehicule from one board cell to down.
        """
        self.setPosY(self.rect.y + self._board.getCellHeight())

    def move(self, direction: Orientations):
        """
        Move the vehicle 1 board cell toward the argument direction.
        """
        if direction == Orientations.TO_LEFT or direction == "TO_LEFT":
            return self.moveLeft()
        if direction == Orientations.TO_RIGHT or direction == "TO_RIGHT":
            return self.moveRight()
        if direction == Orientations.TO_BOTTOM or direction == "TO_BOTTOM":
            return self.moveDown()
        if direction == Orientations.TO_TOP or direction == "TO_TOP":
            return self.moveUp()

        print("Aucun mouvement effectué ce qui n'est pas normal.")

    def __repr__(self):
        return self._name + " -> " + str(self.rect)

    def canMove(self, direction: Orientations, vehiclesList: list["Vehicle"]) -> bool:
        """
        Returns True if the current vehicle can move 1 board cell toward the argument direction.
        Returns False otherwise.
        If the vehiclesList argument is not set, then the board where the movement ability is checked will be the board attribute.
        """
        if self.isVertical() and (direction == Orientations.TO_LEFT or direction == Orientations.TO_RIGHT):
            return False

        if self.isHorizontal() and (direction == Orientations.TO_TOP or direction == Orientations.TO_BOTTOM):
            return False

        # The movement is possible is there is no collision with a wall or another vehicle
        vehicleCopy = self.copy()
        vehicleCopy.move(direction)

        # Vehicle collision
        for vehicle in vehiclesList:
            if vehicle.getId() != vehicleCopy.getId() and vehicleCopy.rect.colliderect(vehicle.rect):
                # print("collision avec" + vehicle.getName())
                return False

        # Wall collision
        if not self._board.getInnerSurface().get_rect().contains(vehicleCopy.rect):
            return False

        del vehicleCopy
        return True

    def display(self):
        self.rotate(self._orientation)
        self.rect = self.image.get_rect()  # The name of this attribute need to be image to correspond to the standards use by pygame.Sprite interface
        self.rect.x, self.rect.y = self._cellPosition

    def drawHeadLights(self):
        headLight1 = pygame.Rect(Vehicle.WIDTH_MARGIN, Vehicle.HEIGHT_MARGIN, self._board.getCellWidth() / 10, self._board.getCellHeight() / 10)
        pygame.draw.rect(self.image, "Red", headLight1, 0, 0, 3, 0, 3, 0)  # Headlight 1
        pygame.draw.rect(self.image, "White", headLight1, 2, 0, 3, 0, 3, 0)  # Headlight 1 outline
        headLight2 = pygame.Rect(Vehicle.WIDTH_MARGIN, self._vehicleHeight - Vehicle.HEIGHT_MARGIN, self._board.getCellWidth() / 10, self._board.getCellHeight() / 10)
        pygame.draw.rect(self.image, "Red", headLight2, 0, 0, 3, 0, 3, 0)  # Headlight 2
        pygame.draw.rect(self.image, "White", headLight2, 2, 0, 3, 0, 3, 0)  # Headlight 2 outline

        pygame.draw.rect(self.image, "White", pygame.Rect(self._vehicleWidth - 30, Vehicle.HEIGHT_MARGIN, 20, self._vehicleHeight - Vehicle.HEIGHT_MARGIN))  # Windscreen rectangle
        pygame.draw.ellipse(self.image, "White", pygame.Rect(self._vehicleWidth - 20, Vehicle.HEIGHT_MARGIN, 20, self._vehicleHeight - Vehicle.HEIGHT_MARGIN))  # Windscreen ellipse

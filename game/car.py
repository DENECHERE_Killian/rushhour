from game.vehicle import *


class Car(Vehicle):

    def __init__(self, carId: int, name: str, color: str, board: Board, orientation: Orientations, cellPosition: tuple[int, int]):
        Vehicle.__init__(self, carId, name, color, 2, board, orientation, cellPosition)
        self.drawVehicle()

    def drawVehicle(self):
        car = pygame.Rect(Vehicle.WIDTH_MARGIN / 2, Vehicle.HEIGHT_MARGIN / 2, self._vehicleWidth, self._vehicleHeight)

        # Creation of the vehicle surface
        self.image = pygame.Surface([self._board.getCellWidth() * self._cellSize, self._board.getCellHeight()]) # The name of this attribute need to be image to correspond to the standards use by pygame.Sprite interface
        self.image.fill("Black") # The background is filled black
        self.image.set_colorkey("Black") # The black is not displayed

        # Draw the car in the surface
        pygame.draw.rect(self.image, self._color, car, 0, 10) # The vehicle as a rectangle
        pygame.draw.rect(self.image, Colors.BLACK if self._color != Colors.BLACK else "White", car, 3, 10) # The vehicle rectangle outline

        self.drawHeadLights()
        self.display()

    def copy(self):
        return Car(self._id, self._name, self._color, self._board, self._orientation, self._cellPosition)

from typing import Union, Optional, Any
from game.car import *
from game.truck import *
from gui.board import Board


class Difficulty:
    """
    Represents the different game difficulty.
    """
    EASY = 0
    MEDIUM = 1
    HARD = 2

    def __init__(self):
        pass


class Game:
    """
    It manage the game functionalities and processes like adding the vehicles, handling vehicles moves, generating a valid game board.
    """

    def __init__(self, board: Board):
        self._board = board
        self._vehiclesList = pygame.sprite.Group() # List of all vehicles placed on the board
        self._selectedVehicle = None
        self._playerMovesNumber = 0
        self._optimalMovesNumber = 0

    # -------
    # GETTERS
    # -------

    def getVehiclesList(self) -> pygame.sprite.Group:
        """
        Returns the list of vehicles added to the board.
        In fact, this is not a list but a pygame.sprite.Group which is grouping multiple pygame.sprite.
        """
        return self._vehiclesList

    def getVehicleCellPositions(self, vehicle: Vehicle) -> Union[Optional[list], Any]:
        """
        Returns the cells list corresponding to the row/column (depending on the vehicle orientation) that the vehicle can move to in theory.
        This method does not check if there is any collision possible between a vehicle of the game and the argument vehicle.
        Globally, this is just the possible positions for this vehicle on his line/column of the board.
        The current position of the argument vehicle is included in this list.
        The order of the cells in the list change in order to start from the cells that the vehicle will move on first:
            - For a |vertical vehicle that is near the |left or right side of the board, the return is a list of the cells starting from the vehicle
            -       |horizontal                        |top or bottom
            - In the case of a vehicle that is in the middle of the board (not near a border), the list will be:
                - Start by the current position cell
                - The cells following the vehicle orientation
                - The cells in the other way (counter orientation)
        """
        if vehicle is None:
            return

        (vehiclePosX, vehiclePosY) = vehicle.getPosition()
        vehicleBoardColumnIndex = int(vehiclePosX / self._board.getCellWidth())
        vehicleBoardLineIndex = int(vehiclePosY / self._board.getCellHeight())

        # print("Le véhicule " + str(vehicle.getName()) + " est placé en ligne " + str(vehicleBoardLineIndex) + " / colonne " + str(vehicleBoardColumnIndex))

        if vehicle.isVertical():
            vehicleBoardColumn = self._board.getColumn(vehicleBoardColumnIndex)
            del vehicleBoardColumn[-(vehicle.getCellSize()-1):] # Remove the positions where the vehicle would be out of the board

            if vehicle.rect.y == 0: # The vehicle is on the top side of the board
                return vehicleBoardColumn
            elif vehicle.rect.y + vehicle.rect.height == self._board.getCellHeight() * self._board.getVerticalCellNumber(): # The vehicle is on the bottom side of the board so reverse the result
                return vehicleBoardColumn[::-1]
            else:
                # The vehicle is not at en extremity of the line.
                # Return the cells that are in front of the vehicle depending on his orientation. Then the cell that are backward the vehicle.
                v = vehicleBoardColumn[vehicleBoardLineIndex]
                beforeCells = vehicleBoardColumn[:vehicleBoardLineIndex][::-1] # From start to vehicle not included (reversed)
                afterCells = vehicleBoardColumn[vehicleBoardLineIndex+1:] # From vehicle not included to end
                return [v] + beforeCells + afterCells if vehicle.getOrientation() == Orientations.TO_TOP else [v] + afterCells + beforeCells
        else:
            vehicleBoardLine = self._board.getLine(vehicleBoardLineIndex)
            del vehicleBoardLine[-(vehicle.getCellSize()-1):] # Remove the positions where the vehicle would be out of the board

            if vehicle.rect.x == 0: # The vehicle is on the left side of the board
                return vehicleBoardLine
            elif vehicle.rect.x + vehicle.rect.width == self._board.getCellWidth() * self._board.getHorizontalCellNumber(): # The vehicle is on the right side of the board so reverse the result
                return vehicleBoardLine[::-1]
            else:
                # The vehicle is not at en extremity of the line.
                # Return the cells that are in front of the vehicle depending on his orientation. Then the cell that are backward the vehicle.
                v = vehicleBoardLine[vehicleBoardColumnIndex]
                beforeCells = vehicleBoardLine[:vehicleBoardColumnIndex] # From start to vehicle not included
                afterCells = vehicleBoardLine[vehicleBoardColumnIndex+1:] # From vehicle not included to end
                return [v] + beforeCells[::-1] + afterCells if vehicle.getOrientation() == Orientations.TO_LEFT else [v] + afterCells + beforeCells[::-1]

    def generateGame(self, difficulty: Difficulty):
        """
        Place several vehicles depending on the chosen difficulty in order to create a valid game board (meaning there is a solution and that the difficulty is respected).
        """
        print("Difficulté -> " + str(difficulty))
        # self.addTruck("Camion vert clair", Vehicle.Colors.LIGHTGREEN, Vehicle.Orientations.TO_LEFT, (2*self.board.cellWidth, 0))
        self.addCar(0, "Voiture rouge", Colors.RED, Orientations.TO_RIGHT, (0, self._board.getExitCellPosition()[1] - self._board.getCellHeight()))
        self.addTruck(1, "Camion noir", Colors.BLACK, Orientations.TO_TOP, (3 * self._board.getCellWidth(), self._board.getExitCellPosition()[1] - self._board.getCellHeight()))
        self.addTruck(2, "Camion jaune", Colors.YELLOW, Orientations.TO_RIGHT, (3 * self._board.getCellWidth(), 5 * self._board.getCellHeight()))

        # Not added because it is paced at the exit position
        # self.addCar("Voiture violette", Colors.PURPLE, Orientations.TO_RIGHT, (5*self.board.cellWidth, self.board.getExitCellPosition()[1] - self.board.cellHeight))
        # Not added because of vehicle collision
        # self.addTruck("Camion bleue clair", Colors.LIGHTBLUE, Orientations.TO_TOP, (3*self.board.cellWidth, self.board.getExitCellPosition()[1] - self.board.cellHeight))

    # -------
    # SETTERS
    # -------

    def setVehiclesList(self, newVehiclesList: list[Vehicle]):
        for vehicle in newVehiclesList:
            vehicleOnBoard = next(filter(lambda v: v.getId() == vehicle.getId(), self._vehiclesList.sprites()))
            vPosX, vPosY = vehicle.getPosition()
            vehicleOnBoard.setPos(vPosX, vPosY)

    # -----------
    # GAME EVENTS
    # -----------

    # Keyboard events
    def keyPressed(self, key: int):
        """
        Handles keyboard events to move vehicles.
        There is a movement if all these condition are respected:
            - A vehicle is selected in order to be moved.
            - The key that was pressed correspond to a direction the vehicle can move on (a vehicle an only go forward or backward corresponding to its spawning orientation).
        It also handles collisions by calling isAnyCollisionExists method.

        Parameters
        ----------
        key: int
            The keyboard pushed key.
        """

        if self._selectedVehicle is None:
            return

        vehicleOrientation = self._selectedVehicle.getOrientation()
        vehicleIsHorizontal = (vehicleOrientation == Orientations.TO_LEFT or vehicleOrientation == Orientations.TO_RIGHT)
        vehicleIsVertical = (vehicleOrientation == Orientations.TO_TOP or vehicleOrientation == Orientations.TO_BOTTOM)

        if key == pygame.K_LEFT and vehicleIsHorizontal:
            self._selectedVehicle.moveLeft()
            if self.isAnyCollisionExists():
                self._selectedVehicle.moveRight()
            else:
                self._playerMovesNumber += 1

        if key == pygame.K_RIGHT and vehicleIsHorizontal:
            self._selectedVehicle.moveRight()
            if self.isAnyCollisionExists():
                self._selectedVehicle.moveLeft()
            else:
                self._playerMovesNumber += 1

        if key == pygame.K_UP and vehicleIsVertical:
            self._selectedVehicle.moveUp()
            if self.isAnyCollisionExists():
                self._selectedVehicle.moveDown()
            else:
                self._playerMovesNumber += 1

        if key == pygame.K_DOWN and vehicleIsVertical:
            self._selectedVehicle.moveDown()
            if self.isAnyCollisionExists():
                self._selectedVehicle.moveUp()
            else:
                self._playerMovesNumber += 1

    # Mouse events
    def mouseClick(self):
        """
        Handles the mouse clicks.
        It is used to select a vehicle in order to be able to move it later.
        """
        mousePosX, mousePosY = pygame.mouse.get_pos()
        for vehicle in self._vehiclesList.sprites():
            boardPosX, boardPosY = self._board.getPosition()
            if vehicle.rect.collidepoint(mousePosX - boardPosX - self._board.getCellWidth(), mousePosY - boardPosY - self._board.getCellHeight()):
                # print("Click sur " + str(vehicle.getName()))

                if self._selectedVehicle is not None:
                    self._selectedVehicle.image.set_alpha(255)  # From 0 to 255

                if self._selectedVehicle != vehicle:
                    self._selectedVehicle = vehicle
                    self._selectedVehicle.image.set_alpha(150) # From 0 to 255
                else:
                    self._selectedVehicle = None

    # -------------
    # OTHER METHODS
    # -------------

    def addCar(self, carId: int, name: str, color: str, orientation: Orientations, position: tuple[int, int]) -> bool:
        """
        Add a car on the list of vehicles we can play with.
        The car is drawn when the update method is called.
        It also check that the added car is not overlapping any other vehicle already placed on the board of if it is placed out of the board by using the isAnyCollisionExists method.
        Make sure the rules about adding a red car to the game are respected:
            - It can only have a unique red car in the game.
            - This car need to be placed horizontally in front of the exit.
            - It's also necessary to have at least one red car in the game (this one is checked by another method)
        """

        if color == Colors.RED:
            for vehicle in self._vehiclesList.sprites():
                if isinstance(vehicle, Car) and vehicle.getColor() == Colors.RED:
                    # The car is not added because there is already a red car in the vehicles list
                    return False

            orientation = Orientations.TO_RIGHT
            position = (position[0], self._board.getExitCellPosition()[1] - self._board.getCellHeight())

        newCar = Car(carId, name, color, self._board, orientation, position)
        self._vehiclesList.add(newCar)
        if self.isAnyCollisionExists(newCar): # The car is placed wrong
            self._vehiclesList.remove(newCar)
            return False
        return True

    def addTruck(self, truckId: int, name: str, color: str, orientation: Orientations, position: tuple[int, int]) -> bool:
        """
        Add a truck on the list of vehicles we can play with.
        The truck is drawn when the update method is called.
        It also check that the added truck is not overlapping any other vehicle already placed on the board of if it is placed out of the board by using the isAnyCollisionExists method.
        """
        newTruck = Truck(truckId, name, color, self._board, orientation, position)
        self._vehiclesList.add(newTruck)
        if self.isAnyCollisionExists(newTruck): # The truck is placed wrong
            self._vehiclesList.remove(newTruck)
            return False
        return True

    def isAnyCollisionExists(self, specificVehicle: Vehicle = None) -> bool:
        """
        Used to check if, after a vehicle movement, any of those does not respect the game rules :
            - No vehicle out of the board.
            - No vehicle overlapping another.
        It also check if the game is win or not yet.
        
        Parameters
        ----------
        specificVehicle : Vehicle
            A vehicle we want to check possible collisions.
            Set to selectedVehicle by default to check collisions for the vehicle that was last moved.
        """
        if specificVehicle is None:
            if self._selectedVehicle is None:
                # It means there is no selected vehicle and no specific vehicle to check the collision with, so it's impossible to have a collision.
                return False
            else:
                specificVehicle = self._selectedVehicle

        # Vehicle collision
        for vehicle in self._vehiclesList.sprites():
            if vehicle != specificVehicle and specificVehicle.rect.colliderect(vehicle.rect):
                # print("collision avec" + vehicle.getName())
                return True # There is a collision

        # Wall collision
        if not self._board.getInnerSurface().get_rect().contains(specificVehicle.rect):
            # If this is the exit and the vehicle is a red car
            exitAsRectangle = self._board.getExitCellAsRectangle()
            if exitAsRectangle.colliderect(specificVehicle.rect) and specificVehicle.getColor() == Vehicle.Colors.RED: # The movement was made by a red car moving to the exit cell position
                # Winning situation
                self.winningSituation()
                return False
            return True
        return False

    def winningSituation(self):
        """
        When the game is won, several actions need to be made:
            - Displaying a victory message
            - Stopping the game (not be able to move vehicles)

        TODO: Ajouter des actions lors de la victoire
        """
        print("The game is over - Well done !")
        print("You found a solution requiring " + str(self._playerMovesNumber) + " movements")
        print("An optimal solution exists requiring " + str(self._optimalMovesNumber) + " movements which is " + str(self._playerMovesNumber - self._optimalMovesNumber) + " less than yours")

    def update(self):
        """
        Update the position af all vehicles displayed on the board.
        """
        self._vehiclesList.clear(self._board.getInnerSurface(), self._board.getInnerSurfaceNeutral()) # Erase the vehicle at his potentially precedent position (if it need to move)
        self._vehiclesList.update() # Update the vehicles positions (automatically call the update method of each vehicle)
        self._vehiclesList.draw(self._board.getInnerSurface()) # Draw the vehicles on the board

import pygame


class Board:
    """
    It represent the game board appearance.
    """

    def __init__(self, boardSize, boardPosition, boardCellNumbers):
        # Board variables
        self._width, self._height = boardSize
        self._positionX, self._positionY = boardPosition
        self._verticalCellNumber, self._horizontalCellNumber = boardCellNumbers
        self._boardSurface = pygame.Surface(boardSize)
        self._cellWidth, self._cellHeight = self.getCellSize()

        # Board drawing variables
        self._innerBoardSurface = pygame.Surface((self._cellWidth * self._verticalCellNumber, self._cellHeight * self._horizontalCellNumber))
        self._innerBoardSurfaceNeutral = None
        self._cellList = [] # List of all cells rectangle. The list items represents all the lines each represented by a list representing a column with pygame.Rect inside.

        self.drawGrid()

    # -------
    # GETTERS
    # -------

    def getCellNumber(self):
        return self._verticalCellNumber, self._horizontalCellNumber

    def getVerticalCellNumber(self):
        return self._verticalCellNumber

    def getHorizontalCellNumber(self):
        return self._horizontalCellNumber

    def getLine(self, index: int):
        if index is None:
            return

        return self._cellList[index]

    def getColumn(self, index: int):
        if index is None:
            return

        column = []
        for line in self._cellList:
            column.append(line[index])
        return column

    def getCellSize(self) -> tuple[int, int]:
        """
        Return the cell width, height of all cells of the game board
        """
        # I've add 2 to each cell number to keep in mind the borders of the game board (innerSurface in drawElements function)
        return self._width / (self._horizontalCellNumber + 2), self._height / (self._verticalCellNumber + 2)

    def getCellWidth(self):
        return self._cellWidth

    def getCellHeight(self):
        return self._cellHeight

    def getExitCellPosition(self) -> tuple[int, int]:
        return (self._horizontalCellNumber + 1) * self._cellWidth, round(self._verticalCellNumber / 2) * self._cellHeight

    def getExitCellAsRectangle(self) -> pygame.Rect:
        exitPosX, exitPosY = self.getExitCellPosition()
        return pygame.Rect((exitPosX - self._cellWidth, exitPosY - self._cellHeight), (self._cellWidth, self._cellHeight))  # We minus the exit coordinates by one cell size because the game board cells coordinates are calculated taking the reference of the inner game board surface. The exit does not initially : it is placed regarding to the game board surface (not the inner one).

    def getInnerSurface(self):
        return self._innerBoardSurface

    def getInnerSurfaceNeutral(self):
        return self._innerBoardSurfaceNeutral

    def getPosition(self):
        return self._positionX, self._positionY

    # -------------
    # OTHER METHODS
    # -------------

    def drawGrid(self):
        self._boardSurface.fill("#AEAAAA")
        self._innerBoardSurface.fill("White")

        # Drawing each cell
        for line in range(0, self._verticalCellNumber):
            self._cellList.append([])
            for column in range(0, self._horizontalCellNumber):
                self._cellList[line].append(self.addCell(self._innerBoardSurface, line, column))

        # Copy to keep a neutral grid that allow us to take parts from in order to erase vehicles after they move
        self._innerBoardSurfaceNeutral = self._innerBoardSurface.copy()

        # Drawing the exit
        exitCellPos = self.getExitCellPosition()
        pygame.draw.rect(self._boardSurface, "Red", [exitCellPos[0], exitCellPos[1], self._cellWidth, self._cellHeight])

    def addCell(self, surface, line, column):
        # In all draw method, I added small int to keep a space between each cells to show borders
        cellRectangle = pygame.Rect(column * self._cellWidth, line * self._cellHeight, self._cellWidth, self._cellHeight)

        if line == self._verticalCellNumber - 1 and column == self._horizontalCellNumber - 1:  # Last board cell (bottom right)
            pygame.draw.rect(surface, "#8497B0", [cellRectangle.x + 2, cellRectangle.y + 2, cellRectangle.width - 4, cellRectangle.height - 4])
        elif line == self._verticalCellNumber - 1:  # Last column cell (bottom)
            pygame.draw.rect(surface, "#8497B0", [cellRectangle.x + 2, cellRectangle.y + 2, cellRectangle.width - 2, cellRectangle.height - 4])
        elif column == self._horizontalCellNumber - 1:  # Last line cell (right)
            pygame.draw.rect(surface, "#8497B0", [cellRectangle.x + 2, cellRectangle.y + 2, cellRectangle.width - 4, cellRectangle.height - 2])
        else:  # Middle board cell
            pygame.draw.rect(surface, "#8497B0", [cellRectangle.x + 2, cellRectangle.y + 2, cellRectangle.width - 2, cellRectangle.height - 2])

        return cellRectangle

    def displayOn(self, surface: pygame.Surface):
        """
        Display the game board on the specified surface
        :param surface: The pygame Surface to display the board on
        """
        self._boardSurface.blit(self._innerBoardSurface, (self._cellWidth, self._cellHeight))
        surface.blit(self._boardSurface, (self._positionX, self._positionY))


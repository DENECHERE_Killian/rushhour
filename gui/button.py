import pygame


class MyButton:
    """
    Create a button, then blit the surface in the while loo
    """
    PADDING = 10

    def __init__(self, text, pos, fontSize, bgColor, textColor, onclick):
        """
        Create e new Button object
        :param text: The text content of the button
        :param pos: (x, y) The position of the text inside his surface
        :param fontSize: The font size the text will be displayed at
        :param bgColor: the background color of the button (black as default)
        :param textColor: The text color
        :param onclick: The function to call when click on the button
        """
        self._posX, self._posY = pos
        self._font = pygame.font.SysFont("Arial", fontSize)
        self._onclick = onclick
        self._bgColor = bgColor
        self._textColor = textColor
        self._surface = None

        self.text = None
        self.rect = None
        self.setText(text)

    def setText(self, text):
        """
        Change the text of the button.
        :param text: The text content of the button
        """
        self.text = self._font.render(text, True, pygame.Color(self._textColor or "White"))
        textSize = self.getSize()
        self._surface = pygame.Surface((textSize[0] + self.PADDING, textSize[1] + self.PADDING))

        self._surface.fill("Red") # The background is filled black
        self._surface.set_colorkey("Red") # The black is not displayed

        pygame.draw.rect(self._surface, self._bgColor, pygame.Rect(0, 0, textSize[0] + self.PADDING, textSize[1] + self.PADDING), 0, 10) # Background color
        self._surface.blit(self.text, (self.PADDING / 2, self.PADDING / 2))
        self.rect = self._surface.get_rect()

    def getSize(self):
        """
        Returns the size of the button (width, height)
        """
        return self.text.get_size()

    def setPosX(self, newPosX):
        """
        Modify the x position of the button
        """
        self._posX = newPosX
        self.rect = self.rect.move(newPosX, self._posY)

    def displayOn(self, surface):
        """
        Displays the button into the screen parameter.
        :param surface: Pygame Surface
        """
        surface.blit(self._surface, (self._posX, self._posY))

    def catchEvent(self, event):
        """
        Bind a click action to the button.
        """
        x, y = pygame.mouse.get_pos()
        if event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed()[0] and self.rect.collidepoint(x, y):
            if self._onclick is not None:  # If a function need to be called when click
                self._onclick()  # Launch the function associated to the button click event
